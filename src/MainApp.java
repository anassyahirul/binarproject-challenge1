import java.util.Scanner;

public class MainApp {

    public static String[] shape2d = {
            "Persegi", "Persegi Panjang", "Segitiga", "Lingkaran"
    };

    public static String[] shape3d = {
            "Kubus", "Balok", "Tabung"
    };

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        viewMainMenu();
    }

    /*
    View Method
     */
    public static void viewMainMenu() {
        while(true) {
            System.out.println("***---------------------------------***");
            System.out.println("Kalkulator Penghitung Luas dan Volume");
            System.out.println("---------------------------------------");

            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volume");
            System.out.println("0. Tutup Aplikasi");

            while(true) {

                System.out.print("Nomor yang dipilih: ");
                Integer choosenNumber = scanner.nextInt();
                System.out.println("***---------------------------------***");
                if (choosenNumber.equals(1)) {
                    view2dShapeMenu();
                    break;
                } else if (choosenNumber.equals(2)) {
                    view3dShapeMenu();
                    break;
                } else if (choosenNumber.equals(0)) {
                    System.out.println("Terimakasih telah menggunakan aplikasi ini\n");
                    System.out.println("Aplikasi ditutup...");
                    System.exit(0);
                } else {
                    System.out.println("Pilihan nomor tidak tersedia, Silakan pilih nomor kembali");
                }

            }
        }
    }

    public static void view2dShapeMenu() {
        System.out.println("***---------------------------------***");
        System.out.println("Pilih Bidang yang akan dihitung");
        System.out.println("---------------------------------------");

        showShapeList(shape2d);
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("---------------------------------------");
        boolean isCorrectNumber = false;
        while (!isCorrectNumber) {
            System.out.print("Nomor Bidang pilihan: ");
            int number = scanner.nextInt();

            System.out.println("***---------------------------------***");

            switch (number) {
                case 1 -> {
                    square();
                    isCorrectNumber = true;
                }
                case 2 -> {
                    rectangle();
                    isCorrectNumber = true;
                }
                case 3 -> {
                    triangle();
                    isCorrectNumber = true;
                }
                case 4 -> {
                    circle();
                    isCorrectNumber = true;
                }
                case 0 -> isCorrectNumber = true;
                default -> System.out.println("Silakan masukkan nomor yang tersedia");
            }

        }
    }

    public static void view3dShapeMenu() {
        System.out.println("***---------------------------------***");
        System.out.println("Pilih Bidang yang akan dihitung");
        System.out.println("---------------------------------------");

        showShapeList(shape3d);
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("---------------------------------------");

        boolean isCorrectNumber = false;
        while (!isCorrectNumber) {
            System.out.print("Nomor Bidang pilihan: ");
            int number = scanner.nextInt();

            System.out.println("***---------------------------------***");

            switch (number) {
                case 1 -> {
                    cube();
                    isCorrectNumber = true;
                }
                case 2 -> {
                    cuboid();
                    isCorrectNumber = true;
                }
                case 3 -> {
                    cylinder();
                    isCorrectNumber = true;
                }
                case 0 -> isCorrectNumber = true;
                default -> System.out.println("Silakan masukkan nomor yang tersedia");
            }
        }
    }

    public static void showShapeList(String[] shapeName) {
        for (int i = 1; i <= shapeName.length; i++) {
            System.out.println(i + ". " + shapeName[i-1]);
        }
    }

    /*
    2d-Shape and 3d-Shape method
     */
    public static void square() {
        headerShape("Persegi");

        System.out.print("Masukkan sisi : ");
        int side = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Luas dari persegi adalah " + calcSquareArea(side));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void rectangle() {
        headerShape("Persegi Panjang");

        System.out.print("Masukkan panjang : ");
        int length = scanner.nextInt();
        System.out.print("Masukkan lebar : ");
        int width = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Luas dari persegi panjang adalah " + calcRectangleArea(length, width));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void triangle() {
        headerShape("Segitiga");

        System.out.print("Masukkan alas : ");
        int base = scanner.nextInt();
        System.out.print("Masukkan tinggi : ");
        int height = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Luas dari segitiga adalah " + calcTriangleArea(base, height));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void circle() {
        headerShape("Lingkaran");

        System.out.print("Masukkan jari-jari : ");
        int radius = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Luas dari lingkaran adalah " + calcCircleArea(radius));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void cube() {
        headerShape("Kubus");

        System.out.print("Masukkan rusuk : ");
        int edge = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Volume dari kubus adalah " + calcCubeVolume(edge));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void cuboid() {
        headerShape("Balok");

        System.out.print("Masukkan panjang : ");
        int length = scanner.nextInt();
        System.out.print("Masukkan lebar : ");
        int width = scanner.nextInt();
        System.out.print("Masukkan tinggi : ");
        int height = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Volume dari balok adalah " + calcCuboidVolume(length, width, height));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void cylinder() {
        headerShape("Tabung");

        System.out.print("Masukkan jari-jari alas : ");
        int radius = scanner.nextInt();
        System.out.print("Masukkan tinggi : ");
        int height = scanner.nextInt();

        System.out.println("\nProcessing...");
        System.out.println("Volume dari tabung adalah " + calcCylinderVolume(radius, height));
        System.out.println("---------------------------------------");
        pressAnyKey();
    }

    public static void headerShape(String shape) {
        System.out.println("---------------------------------------");
        System.out.println("Anda Memilih " + shape);
        System.out.println("---------------------------------------");
    }

    public static void pressAnyKey() {
        System.out.println("Tekan apa saja untuk kembali ke menu utama");
        try{
            System.in.read();
        }
        catch(Exception e){
        }
    }

    /*
    Calculation Method
     */
    public static int calcSquareArea(int side) {
        return side * side;
    }

    public static int calcRectangleArea(int length, int width) {

        return length * width;
    }

    public static double calcTriangleArea(int base, int height) {
        return 0.5 * base * height;
    }

    public static double calcCircleArea(int radius) {

        return 3.14 * radius * radius;
    }

    public static int calcCubeVolume(int edge) {
        return calcSquareArea(edge) * edge;
    }

    public static int calcCuboidVolume(int length, int weight, int height) {
        return calcRectangleArea(length, weight) * height;
    }

    public static double calcCylinderVolume(int radius, int height) {
        return calcCircleArea(radius) * height;
    }

}
